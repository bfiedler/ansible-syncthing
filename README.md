# Syncthing

Deploy syncthing on a set of owned hosts.

## Setup

To use this role you have to generate a new syncthing ID for every participating
host. Do this by starting syncthing in a dummy environment. Then syncthing
creates files in `~/.config/syncthing/`. Copy the content of `key.pem` and
`cert.pem` into the respective host fields in `syncthing_devices`. Then you will
find the syncthing ID inside the `config.xml`. Copy this ID into the `id` field
of the respective device. When copied all this, stop syncthing and remove the
`.config/syncthing` directory. Repeat this for each host you want to have in
your cluster.
